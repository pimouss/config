#!/bin/bash

NEW_USER=$(whoami)

mkdir -p /tmp/install/_home

################################################################################
# Generic config files
################################################################################
cd /tmp/install/_home

wget https://gitlab.com/pimouss/config/raw/master/home/_bashrc_extra
wget https://gitlab.com/pimouss/config/raw/master/home/_inputrc
wget https://gitlab.com/pimouss/config/raw/master/home/_bash_basic_aliases
wget https://gitlab.com/pimouss/config/raw/master/home/_zshrc_extra

cd

cat /tmp/install/_home/_bashrc_extra >> .bashrc
cat /tmp/install/_home/_inputrc > .inputrc
cat /tmp/install/_home/_bash_basic_aliases > ~/.bash_basic_aliases

cat > ~/.bash_aliases << EOF
. ~/.bash_basic_aliases
EOF

################################################################################
# Install and config OhMyZsh
################################################################################
cd 
git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

# in .zshrc : replace ZSH_THEME="robbyrussell" by ZSH_THEME="bira"
# comment line and add
sed -e '/^ZSH_THEME=".*"/ s/^#*/#/' -i ~/.zshrc
sed -i '/^#ZSH_THEME=".*"/a ZSH_THEME="bira"' ~/.zshrc

chsh -s /bin/zsh

cat /tmp/install/_home/_zshrc_extra >> .zshrc

~/glog.py "[$(uname -n)][SUCCESS] $NEW_USER now use ZSH"

################################################################################
# for docker
################################################################################

#sudo apt update
#sudo apt full-upgrade -y
#sudo apt install -y linux-image-generic
#sudo chmod a-x /etc/grub.d/06_OVHkernel
#sudo update-grub
#sudo wget -O /root/install_docker.sh https://gitlab.com/pimouss/config/raw/master/scripts/install_docker.sh
#sudo chmod u+x /root/install_docker.sh
#echo "@reboot /root/install_docker.sh" | sudo crontab -

#~/glog.py "[$(uname -n)][SUCCESS] ready to reboot..."

#sudo reboot


