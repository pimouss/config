#!/bin/bash

NEW_USER=$(whoami)

mkdir -p /tmp/install/_home

################################################################################
# Desktop config files
################################################################################
cd /tmp/install/_home

wget https://gitlab.com/pimouss/config/raw/master/home/_xxshrc_desktop_extra
wget https://gitlab.com/pimouss/config/raw/master/home/_Xdefault_extra
wget https://gitlab.com/pimouss/config/raw/master/home/_xinitrc
wget https://gitlab.com/pimouss/config/raw/master/home/_bash_desktop_aliases

cd

cat /tmp/install/_home/_xxshrc_desktop_extra >> .bashrc
cat /tmp/install/_home/_xxshrc_desktop_extra >> .zshrc

cat /tmp/install/_home/_Xdefaults_extra >> .Xdefaults
cat /tmp/install/_home/_xinitrc > .xinitrc
cat /tmp/install/_home/_bash_desktop_aliases > ~/.bash_desktop_aliases

cat >> ~/.bash_aliases << EOF
. ~/.bash_desktop_aliases
EOF


