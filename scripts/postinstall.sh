#!/bin/bash

NEW_USER="pimouss"
SSH_PUB_KEY_URL="https://gitlab.com/pimouss/pubkeys/raw/master/id_ed25519.pub"
GRAYLOG_SERVER="5.196.27.105"

export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo dpkg-reconfigure -f noninteractive locales

apt-get update
apt-get upgrade -y
apt-get install -y sudo htop vim screen sharutils git zsh curl wget python-pip

pip install djehouty

cat > /root/glog.py << EOF
#!/usr/bin/python
import sys
import logging
from djehouty.libgelf.handlers import GELFTCPSocketHandler

gelf_logger = logging.getLogger('$(uname -n)')
gelf_logger.setLevel(logging.DEBUG)
gelf_logger.addHandler(GELFTCPSocketHandler(
    host            = "$GRAYLOG_SERVER", 
    port            = 12201, 
    static_fields   = {"app": 'glog'}, 
    use_tls         = False,
    level           = logging.DEBUG,
    null_character  = True,
))

gelf_logger.info(sys.argv[1])
EOF

chmod u+x glog.py

./glog.py "[$(uname -n)][SUCCESS] $(uname -n) is up"

echo "#########################################################################################"
echo " new user: $NEW_USER"
echo " get pubkey from: $SSH_PUB_KEY_URL"
echo " pub key: $(curl -s $SSH_PUB_KEY_URL)"
echo "#########################################################################################"

useradd -m -d /home/$NEW_USER -s /bin/bash $NEW_USER
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi

./glog.py "[$(uname -n)][$R] new user $NEW_USER creation"

mkdir -p /home/$NEW_USER/.ssh
chmod 700 /home/$NEW_USER/.ssh
curl -s $SSH_PUB_KEY_URL >> /home/$NEW_USER/.ssh/authorized_keys
chown -R $NEW_USER.$NEW_USER /home/$NEW_USER/.ssh

# authorize sudo for $NEW_USER

if [ -f "/tmp/sudoers.tmp" ]; then
    exit 1
fi
touch /tmp/sudoers.tmp
cp /etc/sudoers /tmp/sudoers.new
cat >> /tmp/sudoers.new << EOF
$NEW_USER ALL=NOPASSWD: ALL
EOF
visudo -c -f /tmp/sudoers.new
if [ "$?" -eq "0" ]; then
    cp /tmp/sudoers.new /etc/sudoers
fi
rm /tmp/sudoers.tmp

./glog.py "[$(uname -n)][SUCCESS] $NEW_USER is now sudo"

# give $NEW_USER the right to chsh
# https://serverfault.com/questions/202468/changing-the-shell-using-chsh-via-the-command-line-in-a-script

# add following line at top of /etc/pam.d/chsh 
#auth       sufficient   pam_wheel.so trust group=chsh
sed  '/^auth       .*/i auth       sufficient pam_wheel.so  trust group=chsh' -i /etc/pam.d/chsh 
groupadd chsh
usermod -a -G chsh $NEW_USER
 
# lock password usage for $NEW_USER
# https://unix.stackexchange.com/questions/210228/add-a-user-wthout-password-but-with-ssh-and-public-key 
usermod --lock $NEW_USER

cp /root/glog.py /home/$NEW_USER/glog.py
chown $NEW_USER.$NEW_USER /home/$NEW_USER/glog.py

wget https://gitlab.com/pimouss/config/raw/master/scripts/postinstall_user.sh

mv postinstall_user.sh /home/$NEW_USER/
chown $NEW_USER.$NEW_USER /home/$NEW_USER/postinstall_user.sh

./glog.py "[$(uname -n)][SUCCESS] entering postinstall_user.sh"

su $NEW_USER /home/$NEW_USER/postinstall_user.sh
