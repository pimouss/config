#!/bin/bash

PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin

~/glog.py "[$(uname -n)][SUCCESS] back to life"

# sed -e '/^\@reboot.*/d' -i /var/spool/cron/crontabs/root
# https://askubuntu.com/questions/408611/how-to-remove-or-delete-single-cron-job-using-linux-command
crontab -r

~/glog.py "[$(uname -n)][SUCCESS] removed all root cron jobs"

uname -r
echo "check" > /root/tututu
#apt-get update
/usr/bin/apt-get install -y apt-transport-https ca-certificates > ~/a1
/usr/bin/apt-key adv \
               --keyserver hkp://ha.pool.sks-keyservers.net:80 \
               --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | tee /etc/apt/sources.list.d/docker.list

/usr/bin/apt-get update > ~/a2
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi
./glog.py "[$(uname -n)][$R] apt-get update"

/usr/bin/apt-get install -y apt-transport-https > ~/a3
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi

./glog.py "[$(uname -n)][$R] apt-get install apt-transport-https"

/usr/bin/apt-get update > ~/a4
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi
./glog.py "[$(uname -n)][$R] apt-get update"

/usr/bin/apt-cache policy docker-engine > ~/a5
/usr/bin/apt-get update > ~/a6
/usr/bin/apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual > ~/a7
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi
./glog.py "[$(uname -n)][$R] apt-get install -y linux-image-extra-$(uname -r) linux-image-extra-virtual"

/usr/bin/apt-get update > ~/a8
/usr/bin/apt-get install -y docker-engine > ~/a9
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi
./glog.py "[$(uname -n)][$R] apt-get install -y docker-engine"


/usr/sbin/service docker start 
if [ $? -eq 0 ]; then
    R="SUCCESS"
else
    R="FAILURE"
fi

./glog.py "[$(uname -n)][$R] docker service started"

groupadd docker
usermod -aG docker pimouss

